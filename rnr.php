<?php


/*
* File rnr.php is designed by Kareem Areola under Remax Integra.
* Unauthorized access or use of this file may be against the law.
* 2017
*
*
* This file is designed to calculate and store data regarding recruiting and retention
*
*/

$startTime = microtime(TRUE);

define('DEBUG_MODE', TRUE);


// Calling requires and getting DB ready...
error_reporting(E_ALL);
require_once('/root/dev/lib/global.functions.php');
require_once('/var/www/common/pdo.class.php');
require_once("/var/www/dev/ka/generate.insert.statement.php");
$agentTotalsTable = "associate_totals";
$db               = "operational";
//exitOnUnauthorized();
$DB = new DB($db, FALSE, TRUE);

$DB->query("use athena_t3");
$DB->execute();

function getData() {

	$DB = $GLOBALS['DB'];
}

	// Table names used throughout the script
	// Totals table
	$t1 = "athena_t1_ifranchise_na.associates_statush";

	// Log variable, store anything to be logged in this variable...
	$log = "";

	//ob_start();


	$startDate = "2017-01-01";
	$endDate   = "2017-06-06";
	$DB->query("TRUNCATE athena_t3.associate_office_status");
	$DB->execute();





	$query = "INSERT INTO athena_t1_ifranchise_na.associate_office_status (`AssociateID`, `OfficeID`,`Status`, `FromDate`, `TillDate`)
 			  SELECT `AssociateID`, `OfficeID`,`Status`, `FromDate`, `TillDate` FROM (SELECT  a.AssociateID, a.Status, b.OfficeID, date(a.StartDate) as FromDate, date(a.EndDate) as TillDate, b.StartDate as officeStartDate, b.EndDate as officeEndDate, b.IsPrimary
   			    FROM  athena_t1_ifranchise_na.associates_statush a
   			    JOIN  athena_t3.na_associates_offices_history b
     		  	  ON  a.AssociateID = b.AssociateID 
       		   	 AND  (date(a.StartDate) = b.StartDate
       			  OR  date(a.EndDate) = b.EndDate)
         	   WHERE  b.isPrimary = 1
          	   ORDER BY AssociateID ASC) as DerrivedTable
 				";
	$DB->query($query);

echo "<br>\n (+) All Rows stored " . runTime($startTime) . "\n";


function cleanup() {

	$DB     = $GLOBALS['DB'];
	$select = "SELECT DISTINCT OfficeID from athena_t1_ifranchise_na.associate_office_status";
	$DB->query($select);
	$DB->execute();
	$res = $DB->getResult();
	foreach ($res as $row) {
		$insert = "insert into athena_t1_ifranchise_na.associate_office_status_temp (select * from athena_t1_ifranchise_na.associate_office_status where OfficeID  = {$row['OfficeID']} group by associateID order by AssociateID, -Till DESC)";
		$DB->query($insert);
		$DB->execute();

	}
}

//getData();


//cleanup();

echo "<br>\n (+) Cleanup Completed " . runTime($startTime) . "\n";


echo "<br>\n (+) SCRIPT COMPLETE " . runTime($startTime) . "\n<br>";
?>