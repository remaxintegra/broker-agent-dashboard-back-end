<?php


/*
* File broker.agent.totals.php is designed by Kareem Areola under Remax Integra.
* Unauthorized access or use of this file may be against the law.
* 2017
*
*
* This file is designed to calculate and store the totals (such as commissions, volume or ends) for agents.
* This data is designed to be leveraged for the Broker Agent Dashboard.
* How to use:
* - This file can run through the browser or scheduled as a cron
* - Two main functions here getLatestData() and updateRanks() do all the work.
* - Main tables used/updated such as $associateTotalsTable are named around line 40
* - Simply call getLatestData($level) with either $level = "agent" or "office" to get the latest agent or office level totals.
* ^ This data is stored in the $associateTotalsTable or the $officeTotalsTable
* - Call updateRanks($level,$locale,$timePeriod) to update ranks (eg. for the "agent" $level and "Office" $locale)
*
*/

$startTime = microtime(TRUE);

define('DEBUG_MODE', TRUE);


// Calling requires and getting DB ready...
error_reporting(E_ALL);
require_once('/root/dev/lib/global.functions.php');
require_once('/var/www/common/pdo.class.php');
require_once("/var/www/dev/ka/generate.insert.statement.php");
$agentTotalsTable = "associate_totals";
$db               = "operational";
//exitOnUnauthorized();
$DB = new DB($db, FALSE, TRUE);

$DB->query("use athena_t3");
$DB->execute();


// Table names used throughout the script
// Totals table
$associateTotalsTable        = "z_associate_totals";
$associateCommissionsTable   = "associate_commissions";
$associateTotalsTable        = "zt";
// Daily rankings table name
$officeTotalsTable       = "zot";
$todayOfficeRankingTable = "zor";
$todayRankingTable       = "zr";

$associateTotalsInsertFields = "TimePeriod,AssociateID,FirstName,LastName,TotalCommission,TotalEnds,TotalVolume,ECR,VolumeEnds,OfficeID,OfficeCity,StateProvinceName,RegionName";
$timePeriods                 = ['YTD', 'LTM'];

// Log variable, store anything to be logged in this variable...
$log = "";

//ob_start();




// Get the latest data and overwrite existing.
function getLatestData($level) {
	// pull all the latest YTD and LTM totals data and  generate insert arrays
	$timePeriods        = $GLOBALS['timePeriods'];
	$officeSummaryTable = "offices_commissions_summary";
	$officeTotalsTable  = $GLOBALS['officeTotalsTable'];

	$associateCommissionsTable = $GLOBALS['associateCommissionsTable'];
	$associateTotalsTable      = $GLOBALS['associateTotalsTable'];
	$DB                        = $GLOBALS['DB'];
	$startTime                 = $GLOBALS['startTime'];
	// Figure out the right start and end times for YTD and LTM
	foreach ($timePeriods as $timePeriod) {



		$endTimeSelect = "SELECT EndThisYear FROM applogs.t3_updated";
		$DB->query($endTimeSelect);
		$endTimeSelectResult = $DB->getResult();
		$endTime             = $endTimeSelectResult[0]['EndThisYear'];

		if ($timePeriod == "YTD") {
			$now       = date('Y-m-d h:i:s');
			$yy        = date('Y');
			$yearStart = "$yy-01-01 00:00:00";
			$t1        = $yearStart;
			$t2        = $endTime;
		} elseif ($timePeriod == "LTM") {
			$nowDt     = date('Y-m-d h:i:s');
			$now       = new DateTime($nowDt);
			$now2      = new DateTime($nowDt);
			$yearStart = $now->modify('-1 year');
			$t1        = $yearStart->format('Y-m-d h:i:s');
			$t2        = $endTime;//$now2->format('Y-m-d h:i:s');
		}
		echo "<br> $timePeriod:   now: $t2<br>";
		echo "<br> $timePeriod:   Start: $t1<br>";

		if ($level == "agent") {

			// Main select statement to generate all agent totals.
			$findTotals = "INSERT INTO `$associateTotalsTable` (FirstName, LastName, TimePeriod, AssociateID, OfficeID, OfficeCity, StateProvinceName, RegionName,TotalCommission, TotalEnds, TotalVolume, VolumeEnds, ECR, ListingBusiness, AvgCommission, AvgVolume, `Timestamp`)
					  (SELECT FirstName, LastName,'$timePeriod' as TimePeriod, AssociateID, OfficeID, OfficeCity, StateProvinceName, RegionName, (SUM(ResCommission)+SUM(ComCommission)) as TotalCommission,
  (SUM(ResSellEnds)+SUM(ResListEnds)+SUM(ComSellEnds)+SUM(ComListEnds)) as TotalEnds,
  (SUM(ResSellVolume)+SUM(ResListVolume)+SUM(ComSellVolume)+SUM(ComListVolume)) as TotalVolume,
  (SUM(ResSellVolume)+SUM(ResListVolume)+SUM(ComSellVolume)+SUM(ComListVolume)) / (SUM(ResSellEnds)+SUM(ResListEnds)+SUM(ComSellEnds)+SUM(ComListEnds)) as VolumeEnds,
  (SUM(ResCommission)+SUM(ComCommission)) / (SUM(ResSellVolume)+SUM(ResListVolume)+SUM(ComSellVolume)+SUM(ComListVolume)) as ECR,
  (SUM(ResSellEnds)+SUM(ComSellEnds)) / (SUM(ResListEnds) + SUM(ComListEnds)) as ListingBusiness,
  (SUM(ResCommission)+SUM(ComCommission)) / (SUM(ResSellEnds)+SUM(ResListEnds)+SUM(ComSellEnds)+SUM(ComListEnds)) as AvgCommission,
  (SUM(ResSellVolume)+SUM(ResListVolume)+SUM(ComSellVolume)+SUM(ComListVolume)) / (SUM(ResSellEnds)+SUM(ResListEnds)+SUM(ComSellEnds)+SUM(ComListEnds)) as AvgVolume,
  						 NOW()
    					 FROM `$associateCommissionsTable` 
 					    WHERE CommissionDate BETWEEN '$t1' AND '$t2' 
					      AND SourceID = 1 
   					 	GROUP BY AssociateID)
   					 	ON DUPLICATE KEY UPDATE
   					 	TotalCommission = TotalCommission,
   					 	TotalEnds = TotalEnds,
   					 	TotalVolume = TotalVolume,
   					 	ECR = ECR,
   					 	VolumeEnds = VolumeEnds,
   					 	ListingBusiness = ListingBusiness,
   					 	AvgVolume = AvgVolume,
   					 	AvgCommission = AvgCommission,
   					 	`Timestamp` = NOW()
   					 	"; // NOTE: This only captures people between the given dates.


			echo "<br>(+)  Querying associate data...  <br>";
			$DB->query($findTotals);
			$DB->execute();
			$count = 0;

			$insert = "INSERT INTO $associateTotalsTable (AssociateID, TimePeriod)
				   (SELECT DISTINCT AssociateID, '$timePeriod' as TimePeriod FROM $associateCommissionsTable )
				   ON DUPLICATE KEY UPDATE
				   zt.AssociateID = zt.AssociateID ";
			$DB->query($insert);
			$DB->execute();


			echo "\n<br>(+)  Query complete for agents  <br>\n";
			echo "<br>\n" . runTime($startTime);



		}
		if ($level == "office") {
			// Main select statement to generate all office totals.
			$findTotals = "INSERT INTO $officeTotalsTable (TimePeriod, OfficeID, OfficeName, OfficeCity, StateProvinceName, RegionName, AvgAgentCount, TotalCommission, TotalEnds, TotalVolume, VolumeEnds,ECR, EndsAgent, CommissionAgent, `Timestamp`)
					  (SELECT '$timePeriod' as TimePeriod, OfficeID, CompanyName, OfficeCity, StateProvinceName, RegionName, AVG(AgentCount) as AvgAgentCount, SUM(Commission) as `TotalCommission`,
 					 	  SUM(Ends) as `TotalEnds`,
 						  SUM(Volume) as `TotalVolume`,
 						  SUM(Volume) / SUM(Ends) as VolumeEnds,
 						  SUM(Commission) / SUM(Volume) as ECR,
 						  SUM(ENDS) / AVG(AgentCount) as EndsAgent,
 						  SUM(Commission) / AVG(AgentCount) as CommissionAgent,
 						  NOW()
 					     FROM $officeSummaryTable 
 					    WHERE CommissionDate BETWEEN '$t1' AND '$t2' 
 					      AND SourceID = 1
 				        GROUP BY OfficeID)
   					 	ON DUPLICATE KEY UPDATE
   					 	TotalCommission = TotalCommission,
   					 	TotalEnds = TotalEnds,
   					 	TotalVolume = TotalVolume,
   					 	ECR = ECR,
   					 	VolumeEnds = VolumeEnds,
   					 	EndsAgent = EndsAgent,
   					 	AvgAgentCount = AvgAgentCount,
   					 	CommissionAgent = CommissionAgent,
   					 	`Timestamp` = NOW()
   					 	"; // NOTE: This only captures people between the given dates.


			echo "<br>(+)  Querying Office data...  <br>";
			$DB->query($findTotals);
			$DB->execute();
			//$res   = $DB->getResult();
			$count = 0;
			echo "\n<br>(+)  Query complete for Offices  <br>\n";
			echo "<br>\n" . runTime($startTime);
		}

	}
	echo "<br> (+) All YTD and LTM associate data inserts complete <br>" . runTime($startTime);
}



// Actually insert all new Ranks
function updateRanks($level, $locale, $timePeriod) {

	//These are the different KPIs that rankings will be generated for
	$rankingsFor             = ["Commission", "Volume", "Ends"];
	// localizing variables into this function
	$todayRankingTable       = $GLOBALS['todayRankingTable'];
	$todayOfficeRankingTable = $GLOBALS['todayOfficeRankingTable'];
	$associateTotalsTable    = $GLOBALS['associateTotalsTable'];
	$officeTotalsTable       = $GLOBALS['officeTotalsTable'];
	$DB                      = $GLOBALS['DB'];

	// The following variables will be used to generate dynamic SQL statements below...
	if ($locale == "Regional") {
		$scopeIdentifier = "Regional";

	}
	if ($locale == "Office") {
		$distinct        = "OfficeID";
		$scopeIdentifier = "Office";
	}
	if ($locale == "City") {
		$distinct        = "OfficeCity";
		$scopeIdentifier = "City";
	}
	if ($locale == "StateProvince") {
		$distinct        = "StateProvinceName";
		$scopeIdentifier = "StateProvince";
	}

	if ($scopeIdentifier == "Regional") {
		$IDs = [""];
	}
	else{
		$q = "SELECT DISTINCT {$distinct} FROM $associateTotalsTable";
		$DB->query($q);
		$res = $DB->getResult();
		foreach ($res as $row) {
			$IDs[] = $row["$distinct"];
		}
	}

	$count = 0;
	// Loop through all office IDs and do the magic
	foreach ($rankingsFor as $rankItem) {
		echo "\n<br> Doing $rankItem for $locale \n<br> Running $level level $locale $timePeriod... ";
		foreach ($IDs as $ID) {
			if ($level == "office") {
				// $scope level $rankItem ranks (eg. City level CommissionRanks)
				// Get the ranks via 'order by' statement
				$select = "SELECT $distinct, OfficeID, OfficeName, Total{$rankItem}
						 FROM $officeTotalsTable 
						WHERE TimePeriod = '$timePeriod'
						  AND $distinct = :distinctItem
						ORDER BY Total{$rankItem} desc ";
				if ($locale == "Regional") {
					$select = "SELECT OfficeID, OfficeName, Total{$rankItem}
						 FROM $officeTotalsTable 
						WHERE TimePeriod = '$timePeriod'						  
						ORDER BY Total{$rankItem} desc ";
				}
				$DB->query($select);
				// If we are not pulling regional data then bind the specific item we are looking for (eg. OfficeCity = $ID)
				if ($locale !="Regional")$DB->bind(":distinctItem", $ID);
				$DB->execute();
				$result = $DB->getResult();
				// Insert / update the commission ranks for offices.
				$rankCounter = 1;
				foreach ($result as $row) {
					$officeID = $row['OfficeID'];
					$officeName = $row['OfficeName'];
					if ($locale == "Regional") {
						$insert = "INSERT INTO $todayOfficeRankingTable (TimePeriod, OfficeID, CompanyName, {$scopeIdentifier}{$rankItem}Rank) 
							  VALUES (:timePeriod,:OfficeID, :OfficeName, :rank)
							  ON DUPLICATE KEY
							  UPDATE {$scopeIdentifier}{$rankItem}Rank = :rank ";
						$DB->query($insert);
					}
					else {
						$insert = "INSERT INTO $todayOfficeRankingTable ($distinct,TimePeriod, OfficeID, CompanyName, {$scopeIdentifier}{$rankItem}Rank) 
							  VALUES (:ID,:timePeriod,:OfficeID, :OfficeName, :rank)
							  ON DUPLICATE KEY
							  UPDATE {$scopeIdentifier}{$rankItem}Rank = :rank ";
						$DB->query($insert);
						echo "\n$insert";
						$DB->bind(":ID", $ID);
					}
					$DB->bind(":timePeriod", $timePeriod);
					$DB->bind(":OfficeID", $officeID);
					$DB->bind(":OfficeName", $officeName);
					$DB->bind(":rank", $rankCounter);
					$DB->execute();
					$rankCounter++;
				}
				$count++;
			}

			if ($level == "agent") {

				// $scope level $rankItem ranks (eg. office level CommissionRanks)
				// Get the ranks via 'order by' statement
				if ($locale == "Regional") {
					$select = "SELECT AssociateID, FirstName, LastName, Total{$rankItem}
						 FROM $associateTotalsTable 
						WHERE TimePeriod = '$timePeriod'
						ORDER BY Total{$rankItem} desc ";
					$DB->query($select);
				}
				else {
					$select = "SELECT $distinct, AssociateID, FirstName, LastName, Total{$rankItem}
						 FROM $associateTotalsTable 
						WHERE $distinct = :ID
						  AND TimePeriod = '$timePeriod'
						ORDER BY Total{$rankItem} desc ";
					$DB->query($select);
					// If we are not pulling regional data then bind the specific item we are looking for (eg. StateProvince = $ID)
					$DB->bind(":ID", $ID);
				}
				$DB->execute();
				$result = $DB->getResult();
				// Insert / update the commission ranks for offices.
				$rankCounter = 1;
				foreach ($result as $row) {
					$assocateID = $row['AssociateID'];
					//$officeID   = $row['OfficeID'];

					if ($locale =="Regional"){
						$insert = "INSERT INTO $todayRankingTable (TimePeriod,AssociateID,FirstName,LastName,{$scopeIdentifier}{$rankItem}Rank) 
							  VALUES (:timePeriod,:AssociateID,:firstName,:lastName,:rank)
							  ON DUPLICATE KEY
							  UPDATE {$scopeIdentifier}{$rankItem}Rank = :rank ";
						$DB->query($insert);
						//echo "\n$insert";
					}
					else {
						$firstname = $row['FirstName'];
						$lastname  = $row['LastName'];
						$insert = "INSERT INTO $todayRankingTable ($distinct,TimePeriod,AssociateID,FirstName,LastName,{$scopeIdentifier}{$rankItem}Rank) 
							  VALUES (:ID,:timePeriod,:AssociateID,:firstName,:lastName,:rank)
							  ON DUPLICATE KEY
							  UPDATE {$scopeIdentifier}{$rankItem}Rank = :rank ";
						$DB->query($insert);
						$DB->bind(":ID", $ID);
						//echo "\n$insert";
					}
					$DB->bind(":timePeriod", $timePeriod);
					$DB->bind(":AssociateID", $assocateID);
					$DB->bind(":firstName", $firstname);
					$DB->bind(":lastName", $lastname);
					$DB->bind(":rank", $rankCounter);
					$DB->execute();
					$rankCounter++;
				}
				$count++;
			}

		}
	}

}

function updateLevelUps(){
	// This function will generate all the data about
	// whatever's required for an agent to reach the next level in ranks
	// (eg. # of deals to reach the next rank in commission up)
	$DB                      = $GLOBALS['DB'];

	$select = "SELECT zr.AssociateID, zr.TimePeriod, zt.TotalCommission, zt.TotalVolume, zt.AvgCommission, zt.AvgVolume, zr.OfficeCommissionRank from zr join zt on zr.AssociateID = zt.AssociateID";
	$DB->query($select);
	$DB->execute();
	$res = $DB->getResult();

	foreach($res as $row){
		$id = $row['AssociateID'];
		$currentAgentVolume = $row['TotalVolume'];
		$currentAgentCommission = $row['TotalCommission'];
		$currentAgentAvgCommission = $row['AvgCommission'];
		$currentAgentAvgVolume = $row['AvgVolume'];
	}
	$select = "SELECT AssociateID, OfficeCommissionRank FROM zr WHERE OfficeComissionRank > (SELECT OfficeCommissionRank FROM zr WHERE AssociateID = $id) limit 1";
	$DB->query($select);
	$DB->execute();
	$res = $DB->getResult();
	$nextID = $res[0]['AssociateID'];
	$select = "";
	foreach($res as $row){
		$nextAgentVolume = $row['TotalVolume'];
		$nextAgentCommission = $row['TotalCommission'];
	}
	$diff = $nextAgentCommission - $currentAgentCommission ;
	$numDealsToNextCommissionRank = $diff / $currentAgentAvgCommission;
	$diff = $nextAgentVolume - $currentAgentVolume;
	$numDealsToNextVolumeRank = $diff / $currentAgentAvgVolume;



}


function genRnRData() {
	// This function will generate all the data about
	// recruiting and retention
	// This will be leveraged to generate the recruiting and retention chart in the Broker Agent Dashboard
	// Data generated here can also be used for other reporting.

	$DB = $GLOBALS['DB'];

	$associateOfficeStatusTable = "athena_t1_ifranchise_na.associate_office_status";

	$DB->query("TRUNCATE $associateOfficeStatusTable");
	$DB->execute();

	$query = "INSERT INTO $associateOfficeStatusTable (`AssociateID`, `OfficeID`,`Status`, `FromDate`, `TillDate`)
 			  SELECT `AssociateID`, `OfficeID`,`Status`, `FromDate`, `TillDate` FROM (SELECT  a.AssociateID, a.Status, b.OfficeID, date(a.StartDate) as FromDate, date(a.EndDate) as TillDate, b.StartDate as officeStartDate, b.EndDate as officeEndDate, b.IsPrimary
   			    FROM  athena_t1_ifranchise_na.associates_statush a
   			    JOIN  athena_t3.na_associates_offices_history b
     		  	  ON  a.AssociateID = b.AssociateID 
       		   	 AND  (date(a.StartDate) = b.StartDate
       			  OR  date(a.EndDate) = b.EndDate)
         	   WHERE  b.isPrimary = 1
          	   ORDER BY AssociateID ASC) as DerrivedTable
 				";
	$DB->query($query);
}

getLatestData("agent");
//getLatestData("office");
//getLatestData("agent");
$locales = ["Office","City","StateProvince","Regional"];
$levels = ["office","agent"];
// Loop to update everything
foreach ($timePeriods as $timePeriod){
	foreach ($levels as $level){
		foreach ($locales as $locale){
			if ($level == "office" && $locale == "Office") continue;
			updateRanks($level,$locale,$timePeriod);
			echo "\n<br>DONE level:$level | locale:$locale | $timePeriod" . runTime($startTime);

		}
	}
}
echo "<br>\n (+) All Rankings COMPLETE " . runTime($startTime);
echo "<br>\n (+) SCRIPT COMPLETE " . runTime($startTime) . "\n<br>";
?>