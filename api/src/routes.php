<?php
// Routes

$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});


$app->get('/getAssociateTotals/{associateID}/{timePeriod}', function ($request, $response, $args) {
	// Sample log message
	$this->logger->info("Slim-Skeleton '/' route");

	$functionRequested = "getAssociateTotals";
	$associateID = $request->getAttribute('associateID');
	$timePeriod = $request->getAttribute('timePeriod');
	//$response->getBody()->write("functionRequested, $functionRequested");
	//$response->getBody()->write("associateID, $associateID");
	//$response->getBody()->write("timePeriod, $timePeriod<br>ARGS: ".var_dump($args));
	//$response->getBody()->write("timePeriod, $timePeriod");

	require_once ('../../Bad.php');

	$bad = new Bad();

	$res = $bad->getAssociateTotalsByID($associateID,$timePeriod);
	$response->getBody()->write(json_encode($res[0]));


	// Render index view
	return $this->renderer->render($response, 'index.phtml', $args);
});

$app->get('/getAssociateRanks/{associateID}/{timePeriod}', function ($request, $response, $args) {
	// Sample log message
	$this->logger->info("Slim-Skeleton '/' route");

	$functionRequested = "getAssociateRanks";
	$associateID = $request->getAttribute('associateID');
	$timePeriod = $request->getAttribute('timePeriod');
	//$response->getBody()->write("functionRequested, $functionRequested");
	//$response->getBody()->write("associateID, $associateID");
	//$response->getBody()->write("timePeriod, $timePeriod<br>ARGS: ".var_dump($args));
	//$response->getBody()->write("timePeriod, $timePeriod");

	require_once ('../../Bad.php');

	$bad = new Bad();

	$res = $bad->getAssociateRankingsByID($associateID,$timePeriod);
	$response->getBody()->write(json_encode($res[0]));


	// Render index view
	return $this->renderer->render($response, 'index.phtml', $args);
});

