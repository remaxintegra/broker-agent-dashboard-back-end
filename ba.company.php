<?php


/*
* File broker.agent.totals.php is designed by Kareem Areola under Remax Integra.
* Unauthorized access or use of this file may be against the law.
* 2017
*
*
* This file is designed to generate a table containing the count of all agents in each province.
*
*/

$startTime = microtime(TRUE);

define('DEBUG_MODE', TRUE);

// Setup DB and pull requires.
error_reporting(E_ALL);
require_once('/root/dev/lib/global.functions.php');
require_once('/var/www/common/pdo.class.php');
require_once("/var/www/dev/ka/generate.insert.statement.php");
$agentTotalsTable = "associate_totals";
$db               = "operational";
//exitOnUnauthorized();
$DB = new DB($db, FALSE, TRUE);

$DB->query("use athena_t3");
$DB->execute();

// Table names used throughout the script
$officeSummaryTable = "offices_commissions_summary";
$officeTotalsTable  = "zot";
$todayRankingTable  = "zor";

$timePeriods = ['LTM', 'YTD'];

// Switches to control which data is pulled
$getLatestData         = 1;
$newRegionalRanks      = 1;
$newStateProvinceRanks = 1;
$newCityRanks          = 1;

// Pull and update totals data with latest info
if ($getLatestData) {
	foreach ($timePeriods as $timePeriod) {

		if ($timePeriod == "YTD") {
			$now       = date('Y-m-d h:i:s');
			$yy        = date('Y');
			$yearStart = "$yy-01-01 00:00:00";
			$t1        = $yearStart;
			$t2        = $now;
		} elseif ($timePeriod == "LTM") {
			$nowDt     = date('Y-m-d h:i:s');
			$now       = new DateTime($nowDt);
			$now2      = new DateTime($nowDt);
			$yearStart = $now->modify('-1 year');
			$t1        = $yearStart->format('Y-m-d h:i:s');
			$t2        = $now2->format('Y-m-d h:i:s');
		}

		// Main query to find all Company totals
		$findTotals = "SELECT CommissionDate, OfficeID, OfficeCity, StateProvinceName, RegionName, AVG(AgentCount) as AvgAgentCount, SUM(Commission) as `TotalCommission`,
 					 	  SUM(Ends) as `TotalEnds`,
 						  SUM(Volume) as `TotalVolume`
 					     FROM $officeSummaryTable 
 					    WHERE CommissionDate BETWEEN '$t1' AND '$t2' 
 					      AND SourceID = 1
 				        GROUP BY OfficeID";
		$DB->query($findTotals);
		$DB->execute();
		$res = $DB->getResult();

		// Generate Inserts for totals
		foreach ($res as $val) {
			// TODO: Ends/Agent |  Commission/Agent
			$insertArray['TimePeriod']      = $timePeriod;
			$insertArray['TotalCommission'] = $val['TotalCommission'];
			$insertArray['TotalEnds']       = $val['TotalEnds'];
			$insertArray['TotalVolume']     = $val['TotalVolume'];
			$insertArray['ECR']             = $val['TotalCommission'] / $val['TotalVolume'];
			$insertArray['VolumeEnds']      = $val['TotalVolume'] / $val['TotalEnds'];
			//$insertArray['CommissionAgent']   = $val['TotalCommission'] / $val['TotalEnds'];
			$insertArray['AvgAgentCount']     = $val['AvgAgentCount'];
			$insertArray['OfficeID']          = $val['OfficeID'];
			$insertArray['OfficeCity']        = $val['OfficeCity'];
			$insertArray['StateProvinceName'] = $val['StateProvinceName'];
			$insertArray['RegionName']        = $val['RegionName'];
			$inserts[] = generateInsertStatement($officeTotalsTable, $insertArray);
		}
		echo "<br> Insert Array preparation complete <br>" . runTime($startTime);
		foreach ($inserts as $insert) {
			echo "<br> $insert ";

			$DB->query($insert);
			$DB->execute();
		}
		echo "<br> (+) All YTD and LTM Office data inserts complete <br>" . runTime($startTime);
	}
}

// Update Region level commission ranks for offices
if ($newRegionalRanks) {
	foreach ($timePeriods as $timePeriod) {
		$select = "SELECT OfficeID, TotalCommission, StateProvinceName 
					 FROM $officeTotalsTable 
					WHERE TimePeriod  = '$timePeriod'
					ORDER BY TotalCommission DESC";
		//die($select);
		$DB->query($select);
		$DB->execute();
		$res = $DB->getResult();

		//Insert update Regional Commission rank
		$rankCounter = 1;
		foreach ($res as $row) {
			$officeID          = $row['OfficeID'];
			$stateProvinceName = $row['StateProvinceName'];
			$insert            = "INSERT INTO $todayRankingTable (`TimePeriod`,`OfficeID`,`RegionalCommissionRank`,`StateProvinceName`) 
						   VALUES('$timePeriod','$officeID',$rankCounter,'$stateProvinceName')
						   ON DUPLICATE KEY
						   UPDATE RegionalCommissionRank = $rankCounter";

			echo "<br> $insert <br>";

			$DB->query($insert);
			$DB->execute();
			$rankCounter++;
		}
		echo "<br> Commission $timePeriod Rankings COMPLETE " . runTime($startTime);
		// Selecting total Ends
		$select = "SELECT  OfficeID, TotalEnds 
 					 FROM $officeTotalsTable 
 					WHERE TimePeriod  = '$timePeriod' 
 					ORDER BY TotalEnds DESC";
		$DB->query($select);
		$DB->execute();
		$res = $DB->getResult();
		// Updating total Ends Rank counts
		$rankCounter = 1;
		foreach ($res as $row) {
			$officeID = $row['OfficeID'];
			$insert   = "INSERT INTO $todayRankingTable (`TimePeriod`,`OfficeID`,`RegionalEndsRank`)
 						   VALUES('$timePeriod','$officeID',$rankCounter)
 						   ON DUPLICATE KEY
   						   UPDATE RegionalEndsRank = $rankCounter";
			echo "<br> $insert <br>";
			$DB->query($insert);
			$DB->execute();
			$rankCounter++;
		}
		echo "<br> Ends $timePeriod regional rankings COMPLETE " . runTime($startTime);
		$select = "SELECT OfficeID,TotalVolume 
				 	 FROM $officeTotalsTable 
				 	WHERE TimePeriod  = '$timePeriod' 
				    ORDER BY TotalVolume DESC";
		$DB->query($select);
		$DB->execute();
		$res = $DB->getResult();
		$rankCounter = 1;
		foreach ($res as $row) {
			$officeID = $row['OfficeID'];
			$insert   = "INSERT INTO $todayRankingTable (`TimePeriod`,`OfficeID`,`RegionalVolumeRank`) 
						  VALUES('$timePeriod','$officeID',$rankCounter)
						  ON DUPLICATE KEY
						  UPDATE RegionalVolumeRank = $rankCounter";
			echo "<br> $insert <br>";
			$DB->query($insert);
			$DB->execute();
			$rankCounter++;
		}

	}
	echo "<br> Completed Regional rankings";
}

// Update data with new State Province rankings
if ($newStateProvinceRanks) {
	// Get all State Province names
	foreach ($timePeriods as $timePeriod) {
		$select = "SELECT DISTINCT StateProvinceName 
					 FROM $todayRankingTable";
		$DB->query($select);
		$DB->execute();
		$res = $DB->getResult();
		foreach ($res as $row) {
			$stateProvinces[] = $row['StateProvinceName'];
		}
		foreach ($stateProvinces as $stateProvince) {
			// Provincial level Commission ranks
			if ($stateProvince == "") continue;
			$select = "SELECT StateProvinceName, OfficeID,  TotalCommission 
						 FROM $officeTotalsTable 
						WHERE StateProvinceName = '$stateProvince' 
						ORDER BY TotalCommission desc ";
			echo "$select";
			$DB->query($select);
			$DB->execute();
			$res = $DB->getResult();
			echo "<br>";
			$rankCounter = 1;
			foreach ($res as $row) {
				echo "<br>";
				$officeID  = $row['OfficeID'];
				$inserts[] = $insert = "INSERT INTO $todayRankingTable (`TimePeriod`,`OfficeID`,`StateProvinceCommissionRank`,`StateProvinceName`) 
							  VALUES ('$timePeriod','$officeID',$rankCounter,'$stateProvince')
							  ON DUPLICATE KEY
							  UPDATE StateProvinceCommissionRank = $rankCounter ";
				echo "<br>";
				echo "$insert";
				echo "<br>";
				$DB->query($insert);
				$DB->execute();
				$rankCounter++;
			}

			// Provincial level Volume ranks

			$select = "SELECT StateProvinceName, OfficeID, TotalVolume 
						 FROM $officeTotalsTable 
						WHERE StateProvinceName = '$stateProvince' 
						ORDER BY TotalVolume desc ";
			echo "$select";
			$DB->query($select);
			$DB->execute();
			$res = $DB->getResult();

			$rankCounter = 1;
			foreach ($res as $row) {

				$officeID  = $row['OfficeID'];
				$inserts[] = $insert = "INSERT INTO $todayRankingTable (`TimePeriod`,`OfficeID`,`StateProvinceVolumeRank`) 
							  VALUES ('$timePeriod','$officeID',$rankCounter)
							  ON DUPLICATE KEY
							  UPDATE StateProvinceVolumeRank = $rankCounter ";

				echo "<br>";
				echo "$insert";
				echo "<br>";

				$DB->query($insert);
				$DB->execute();
				$rankCounter++;
			}

			// Provincial level Ends ranks

			$select = "SELECT StateProvinceName, OfficeID, TotalEnds 
						 FROM $officeTotalsTable 
						WHERE StateProvinceName = '$stateProvince' 
						ORDER BY TotalEnds desc ";
			echo "$select";
			$DB->query($select);
			$DB->execute();
			$res = $DB->getResult();
			
			$rankCounter = 1;
			foreach ($res as $row) {
				echo "<br>";
				$officeID  = $row['OfficeID'];
				$inserts[] = $insert = "INSERT INTO $todayRankingTable (`TimePeriod`,`OfficeID`,`StateProvinceEndsRank`) 
							  VALUES ('$timePeriod','$officeID',$rankCounter)
							  ON DUPLICATE KEY
							  UPDATE StateProvinceEndsRank = $rankCounter ";
				echo "<br>";
				echo "$insert";
				echo "<br>";
				$DB->query($insert);
				$DB->execute();
				$rankCounter++;
			}

		}

	}

}

// Update with new City wide ranks
if ($newCityRanks) {

	echo "<br> Running this <br>";

	foreach ($timePeriods as $timePeriod) {

		$select = "SELECT DISTINCT OfficeCity 
					 FROM $officeTotalsTable";
		$DB->query($select);
		$DB->execute();
		$res = $DB->getResult();

		// Collect all unique cities into an array
		foreach ($res as $row) {
			$cities[] = addslashes($row['OfficeCity']);
		}

		foreach ($cities as $city) {

			// Provincial level Commission ranks

			if ($city == "") continue;
			$select = "SELECT OfficeCity, OfficeID, TotalCommission 
						 FROM $officeTotalsTable 
						WHERE OfficeCity = '$city' 
						ORDER BY TotalCommission desc ";
			echo "$select";
			$DB->query($select);
			$DB->execute();
			$res = $DB->getResult();

			echo "<br>";

			// Update City commission ranks
			$rankCounter = 1;
			foreach ($res as $row) {
				echo "<br>";
				$officeID  = $row['OfficeID'];
				$inserts[] = $insert = "INSERT INTO $todayRankingTable (`TimePeriod`,`OfficeID`,`CityCommissionRank`,`OfficeCity`) 
							  VALUES ('$timePeriod','$officeID',$rankCounter,'$city')
							  ON DUPLICATE KEY
							  UPDATE CityCommissionRank = $rankCounter ";

				echo "<br>";
				echo "$insert";
				echo "<br>";

				$DB->query($insert);
				$DB->execute();
				//$DB->getResult();

				$rankCounter++;
			}

			// Provincial level Volume ranks

			$select = "SELECT OfficeCity, OfficeID, TotalVolume 
						 FROM $officeTotalsTable 
						WHERE OfficeCity = '$city' 
						ORDER BY TotalVolume desc ";
			echo "$select";
			$DB->query($select);
			$DB->execute();
			$res = $DB->getResult();

			echo "<br>";

			// Update City Volume Ranks
			$rankCounter = 1;
			foreach ($res as $row) {
				echo "<br>";
				$officeID  = $row['OfficeID'];
				$inserts[] = $insert = "INSERT INTO $todayRankingTable (`TimePeriod`,`OfficeID`,`CityVolumeRank`) 
							  VALUES ('$timePeriod','$officeID',$rankCounter)
							  ON DUPLICATE KEY
							  UPDATE CityVolumeRank = $rankCounter ";

				echo "<br>";
				echo "$insert";
				echo "<br>";

				$DB->query($insert);
				$DB->execute();
				//$DB->getResult();

				$rankCounter++;
			}

			// Provincial level Ends ranks

			$select = "SELECT OfficeCity, OfficeID, TotalEnds 
						 FROM $officeTotalsTable 
						WHERE OfficeCity = '$city' 
						ORDER BY TotalEnds desc ";
			echo "$select";
			$DB->query($select);
			$DB->execute();
			$res = $DB->getResult();

			echo "<br>";

			// Update City Ends ranks
			$rankCounter = 1;
			foreach ($res as $row) {
				echo "<br>";
				$officeID  = $row['OfficeID'];
				$inserts[] = $insert = "INSERT INTO $todayRankingTable (`TimePeriod`,`OfficeID`,`CityEndsRank`) 
							  VALUES ('$timePeriod','$officeID',$rankCounter)
							  ON DUPLICATE KEY
							  UPDATE CityEndsRank = $rankCounter ";
				echo "<br>";
				echo "$insert";
				echo "<br>";
				$DB->query($insert);
				$DB->execute();
				//$DB->getResult();

				$rankCounter++;
			}

		}

	}
}


echo "<br> Done all Office Level rankings<br>";
echo "<br>" . runTime($startTime) . "<br>";





