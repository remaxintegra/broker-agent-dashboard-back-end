<?php

// NG: need to utilize PDO
// Avoid using "SELECT *" - only select what's needed

class Bad {
	protected $DB;
	protected $agentrankingsTable;
	protected $agentTotalsTable;
	protected $officeTotalsTable;

	function __construct() {
		require_once('/var/www/common/pdo.class.php');
		$db                       = "operational";
		$this->agentrankingsTable = "athena_t3.zr";
		$this->agentTotalsTable   = "athena_t3.zt";
		$this->officeTotalsTable   = "athena_t3.zot";
		$this->officeRankingsTable   = "athena_t3.zor";
		$this->DB                 = new DB($db, FALSE, TRUE);
		$this->DB->query("use athena_t3");            // NG: not needed - you can provide the database name when initializing the class
	}

	private function getData($query) {
		// All other functions will pass a string containing to this, and this function will simply return the result
		// This will reduce "redundant" code from other functions
		$DB = $this->DB;
		$DB->query($query);
		$DB->execute();
		return $res = $DB->getResult();
	}

	public function getAllAssociates($withoutNulls) {
		// Returns a list of all AssociateIDs and agent names.
		// Pass "withoutNull" as TRUE/FALSE to only get agents with non null values.
		$where= "";
		if ($withoutNulls) $where = "WHERE TotalCommission != 0";
		$selectAgents = "SELECT DISTINCT AssociateID, FirstName, LastName 
						   FROM {$this->agentTotalsTable} $where ORDER BY FirstName ASC ";
		return $this->getData($selectAgents);
	}



	public function getAssociateTotalsByID($associateID, $timePeriod) {
		// Pass this function an AssociateID and Time period and it returns all the production totals for the given associate.
		$associateID  = addslashes($associateID);
		$selectAgents = "SELECT * 
					 	   FROM {$this->agentTotalsTable} 
						  WHERE AssociateID = '$associateID' AND TimePeriod = '$timePeriod'";
		return $this->getData($selectAgents);
	}

	public function getAssociateRankingsByID($associateID, $timePeriod) {
		// Pass this function an AssociateID and Time period and it returns all the rankings for the given associate.
		$associateID  = addslashes($associateID);
		$selectAgents = "SELECT * 
						   FROM {$this->agentrankingsTable} 
						  WHERE AssociateID = '$associateID' 
						    AND TimePeriod = '$timePeriod'";
		return $this->getData($selectAgents);
	}

	public function getOfficeTotals($officeID, $timePeriod) {
		// Pass this function an OfficeID and Time period and it returns all the production totals for the given Office.
		$officeID = addslashes($officeID);
		$q        = "SELECT * 
                FROM {$this->officeTotalsTable}
               WHERE TimePeriod = '$timePeriod'
                 AND OfficeID = '$officeID'";
		return $this->getData($q);
	}
	public function getOfficeRankings($officeID,$timePeriod) {
		// Pass this function an OfficeID and Time period and it returns all the Rankings for the given Office.
		$officeID = addslashes($officeID);
		$q        = "SELECT * 
                FROM {$this->officeRankingsTable}
               WHERE TimePeriod = '$timePeriod'
                 AND OfficeID = '$officeID'";
		return $this->getData($q);
	}
}

?>
