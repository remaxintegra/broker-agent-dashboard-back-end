<?php


/*
* File broker.agent.totals.php is designed by Kareem Areola under Remax Integra.
* Unauthorized access or use of this file may be against the law.
* 2017
*
*
* This file is designed to calculate and store the totals (such as commissions, volume or ends) for agents.
* This data is designed to be leveraged for the Broker Agent Dashboard.
* How to use:
 * This file can run through the browser or scheduled as a cron
 * The switches after line 50 are used to control which data will be pulled by the script.
 * Main tables used/updated are $associateTotalsTable and $todayRankingTable
*
*/

$startTime = microtime(TRUE);

define('DEBUG_MODE', TRUE);


// Calling requires and getting DB ready...
error_reporting(E_ALL);
require_once('/root/dev/lib/global.functions.php');
require_once('/var/www/common/pdo.class.php');
require_once("/var/www/dev/ka/generate.insert.statement.php");
$agentTotalsTable = "associate_totals";
$db               = "operational";
//exitOnUnauthorized();
$DB = new DB($db, FALSE, TRUE);

$DB->query("use athena_t3");
$DB->execute();


// Table names used throughout the script
// Totals table
$associateTotalsTable        = "z_associate_totals";
$associateTotalsTable        = "zt";
$associateTotalsInsertFields = "TimePeriod,AssociateID,FirstName,LastName,TotalCommission,TotalEnds,TotalVolume,ECR,VolumeEnds,OfficeID,OfficeCity,StateProvinceName,RegionName";
$timePeriods                 = ['YTD', 'LTM'];
$associateCommissionsTable   = "associate_commissions";

// Daily rankings table name
$officeTotalsTable       = "zot";
$todayOfficeRankingTable = "zor";
$todayRankingTable       = "zr";

// Log variable, store anything to be logged in this variable...
$log = "";


// Switches to control which data is pulled and updated through the script.
$getLatestData         = 0;
$newRegionalRanks      = 0;
$newOfficeRanks        = 0;
$newStateProvinceRanks = 0;
$newCityRanks          = 0;

//ob_start();

// pull all the latest YTD and LTM totals data and  generate insert arrays

// Get the latest data and overwrite existing.
function getLatestData($level) {

	$timePeriods        = $GLOBALS['timePeriods'];
	$officeSummaryTable = "offices_commissions_summary";
	$officeTotalsTable  = $GLOBALS['officeTotalsTable'];

	$associateCommissionsTable = $GLOBALS['associateCommissionsTable'];
	$associateTotalsTable      = $GLOBALS['associateTotalsTable'];
	$DB                        = $GLOBALS['DB'];
	$startTime                 = $GLOBALS['startTime'];
	// Figure out the right start and end times for YTD and LTM
	foreach ($timePeriods as $timePeriod) {

		$endTimeSelect = "SELECT EndThisYear FROM applogs.t3_updated";
		$DB->query($endTimeSelect);
		$endTimeSelectResult = $DB->getResult();
		$endTime             = $endTimeSelectResult[0]['EndThisYear'];

		if ($timePeriod == "YTD") {
			$now       = date('Y-m-d h:i:s');
			$yy        = date('Y');
			$yearStart = "$yy-01-01 00:00:00";
			$t1        = $yearStart;
			$t2        = $endTime;
		} elseif ($timePeriod == "LTM") {
			$nowDt     = date('Y-m-d h:i:s');
			$now       = new DateTime($nowDt);
			$now2      = new DateTime($nowDt);
			$yearStart = $now->modify('-1 year');
			$t1        = $yearStart->format('Y-m-d h:i:s');
			$t2        = $endTime;//$now2->format('Y-m-d h:i:s');
		}
		echo "<br> $timePeriod:   now: $t2<br>";
		echo "<br> $timePeriod:   Start: $t1<br>";

		if ($level == "agent") {

			// Main select statement to generate all agent totals.
			$findTotals = "INSERT INTO $associateTotalsTable (FirstName, LastName, TimePeriod, AssociateID, OfficeID, OfficeCity, StateProvinceName, RegionName,TotalCommission, TotalEnds, TotalVolume, VolumeEnds,ECR)
					  (SELECT FirstName, LastName,'$timePeriod' as TimePeriod, AssociateID, OfficeID, OfficeCity, StateProvinceName, RegionName, (SUM(ResCommission)+SUM(ComCommission)) as TotalCommission,
  (SUM(ResSellEnds)+SUM(ResListEnds)+SUM(ComSellEnds)+SUM(ComListEnds)) as TotalEnds,
  (SUM(ResSellVolume)+SUM(ResListVolume)+SUM(ComSellVolume)+SUM(ComListVolume)) as TotalVolume,
  (SUM(ResSellVolume)+SUM(ResListVolume)+SUM(ComSellVolume)+SUM(ComListVolume)) / (SUM(ResSellEnds)+SUM(ResListEnds)+SUM(ComSellEnds)+SUM(ComListEnds)) as VolumeEnds,
  (SUM(ResCommission)+SUM(ComCommission)) / (SUM(ResSellVolume)+SUM(ResListVolume)+SUM(ComSellVolume)+SUM(ComListVolume)) as ECR
    					 FROM $associateCommissionsTable 
 					    WHERE CommissionDate BETWEEN '$t1' AND '$t2' 
					      AND SourceID = 1 
   					 	GROUP BY AssociateID)
   					 	ON DUPLICATE KEY UPDATE
   					 	TotalCommission = TotalCommission,
   					 	TotalEnds = TotalEnds,
   					 	TotalVolume = TotalVolume,
   					 	ECR = ECR,
   					 	VolumeEnds = VolumeEnds
   					 	"; // NOTE: This only captures people between the given dates.
			// TODO: Fix above query to capture everyone despite given dates and use where instead of group by for more reliable results

			echo "<br>(+)  Querying associate data...  <br>";
			$DB->query($findTotals);
			$DB->execute();
			//$res   = $DB->getResult();
			$count = 0;

			echo "\n<br>(+)  Query complete for agents  <br>\n";

			echo "<br>\n" . runTime($startTime);

		}
		if ($level == "office") {

			// Main select statement to generate all agent totals.
			$findTotals = "INSERT INTO $officeTotalsTable (TimePeriod, OfficeID, OfficeCity, StateProvinceName, RegionName, AvgAgentCount, TotalCommission, TotalEnds, TotalVolume, VolumeEnds,ECR)
					  (SELECT '$timePeriod' as TimePeriod, OfficeID, OfficeCity, StateProvinceName, RegionName, AVG(AgentCount) as AvgAgentCount, SUM(Commission) as `TotalCommission`,
 					 	  SUM(Ends) as `TotalEnds`,
 						  SUM(Volume) as `TotalVolume`,
 						  SUM(Volume) / SUM(Ends) as VolumeEnds,
 						  SUM(Commission) / SUM(Volume) as ECR
 					     FROM $officeSummaryTable 
 					    WHERE CommissionDate BETWEEN '$t1' AND '$t2' 
 					      AND SourceID = 1
 				        GROUP BY OfficeID)
   					 	ON DUPLICATE KEY UPDATE
   					 	TotalCommission = TotalCommission,
   					 	TotalEnds = TotalEnds,
   					 	TotalVolume = TotalVolume,
   					 	ECR = ECR,
   					 	VolumeEnds = VolumeEnds,
   					 	AvgAgentCount = AvgAgentCount
   					 	"; // NOTE: This only captures people between the given dates.
			// TODO: Fix above query to capture everyone despite given dates and use where instead of group by for more reliable results

			echo "<br>(+)  Querying Office data...  <br>";
			$DB->query($findTotals);
			$DB->execute();
			//$res   = $DB->getResult();
			$count = 0;

			echo "\n<br>(+)  Query complete for Offices  <br>\n";

			echo "<br>\n" . runTime($startTime);

		}
	}


	echo "<br> (+) All YTD and LTM associate data inserts complete <br>" . runTime($startTime);
}

// Actually insert all new Regionalranks


function updateRanks($level, $locale, $timePeriod) {

	$rankingsFor             = ["Commission", "Volume", "Ends"];
	$todayRankingTable       = $GLOBALS['todayRankingTable'];
	$todayOfficeRankingTable = $GLOBALS['todayOfficeRankingTable'];
	$associateTotalsTable    = $GLOBALS['associateTotalsTable'];
	$DB                      = $GLOBALS['DB'];


	if ($locale == "Regional") {
		$scopeIdentifier = "Regional";

	}
	if ($locale == "Office") {
		$distinct        = "OfficeID";
		$scopeIdentifier = "Office";
	}
	if ($locale == "City") {
		$distinct        = "OfficeCity";
		$scopeIdentifier = "City";
	}
	if ($locale == "StateProvince") {
		$distinct        = "StateProvinceName";
		$scopeIdentifier = "StateProvince";
	}

	if ($scopeIdentifier == "Regional") $IDs = [""];
	else{
		$q = "SELECT DISTINCT {$distinct} FROM $associateTotalsTable";
		echo "\n<br>$q";
		$DB->query($q);
		$res = $DB->getResult();

		foreach ($res as $row) {
			$IDs[] = $row["$distinct"];
		}
	}

		$count = 0;

		// Loop through all office IDs and do the magic
		foreach ($rankingsFor as $rankItem) {

			echo "\n<br> Doing $rankItem for $locale ";

			foreach ($IDs as $ID) {



				if ($level == "office") {
					// $scope level Commission ranks
					// Get all the total commissions

					echo "\n<br> Running office level... ";


					$select = "SELECT $distinct, OfficeID, Total{$rankItem}
						 FROM $associateTotalsTable 
						WHERE TimePeriod = '$timePeriod'
						  AND $distinct = :distinctItem
						ORDER BY Total{$rankItem} desc ";
					if ($locale == "Regional") {
						$select = "SELECT OfficeID, Total{$rankItem}
						 FROM $associateTotalsTable 
						WHERE TimePeriod = '$timePeriod'						  
						ORDER BY Total{$rankItem} desc ";
					}
					$DB->query($select);
					if ($locale !="Regional")$DB->bind(":distinctItem", $ID);

					echo "\n<br>Running for $ID\n<br>" . $DB->debugDumpParams();

					$DB->execute();
					$result = $DB->getResult();

					// Insert / update the commission ranks for offices.
					$rankCounter = 1;
					foreach ($result as $row) {
						$officeID = $row['OfficeID'];
						if ($locale == "Regional") {
							$insert = "INSERT INTO $todayOfficeRankingTable (TimePeriod, OfficeID,{$scopeIdentifier}{$rankItem}Rank) 
							  VALUES (:timePeriod,:OfficeID,:rank)
							  ON DUPLICATE KEY
							  UPDATE {$scopeIdentifier}{$rankItem}Rank = :rank ";
						}
						else {
							$insert = "INSERT INTO $todayOfficeRankingTable ($distinct,TimePeriod, OfficeID,{$scopeIdentifier}{$rankItem}Rank) 
							  VALUES (:ID,:timePeriod,:OfficeID,:rank)
							  ON DUPLICATE KEY
							  UPDATE {$scopeIdentifier}{$rankItem}Rank = :rank ";
							$DB->bind(":ID", $ID);
						}
							$DB->query($insert);
							$DB->bind(":timePeriod", $timePeriod);
							$DB->bind(":OfficeID", $officeID);
							$DB->bind(":rank", $rankCounter);

							$DB->execute();
							$rankCounter++;

					}
					$count++;
					//if ($count == 1) die("<br>DONE" . runTime($GLOBALS['startTime']));
				}


				if ($level == "agent") {

					echo "\n<br> Running agent level $timePeriod... ";

					// $scope level Commission ranks
					// Get all the total commissions
					if ($locale == "Regional") {
						$select = "SELECT AssociateID, FirstName, LastName, Total{$rankItem}
						 FROM $associateTotalsTable 
						WHERE TimePeriod = '$timePeriod'
						ORDER BY Total{$rankItem} desc ";
					}
					else {
						$select = "SELECT $distinct, AssociateID, FirstName, LastName, Total{$rankItem}
						 FROM $associateTotalsTable 
						WHERE $distinct = :ID
						  AND TimePeriod = '$timePeriod'
						ORDER BY Total{$rankItem} desc ";
						$DB->bind(":ID", $ID);
					}
					echo "\n<br>$select";
					$DB->query($select);
					$DB->execute();
					$result = $DB->getResult();

					// Insert / update the commission ranks for offices.
					$rankCounter = 1;
					foreach ($result as $row) {
						$assocateID = $row['AssociateID'];
						//$officeID   = $row['OfficeID'];

						if ($locale =="Regional"){}
						else {
							$firstname = addslashes($row['FirstName']);
							$lastname  = addslashes($row['LastName']);
							$insert = "INSERT INTO $todayRankingTable ($distinct,TimePeriod,AssociateID,FirstName,LastName,{$scopeIdentifier}{$rankItem}Rank) 
							  VALUES ('$ID','$timePeriod','$assocateID','$firstname','$lastname',$rankCounter)
							  ON DUPLICATE KEY
							  UPDATE {$scopeIdentifier}{$rankItem}Rank = $rankCounter ";
						}
						echo "\n<br>$insert";
						$DB->query($insert);
						$DB->execute();
						$rankCounter++;
					}
					$count++;
					//if ($count == 1) die("<br>DONE" . runTime($GLOBALS['startTime']));
				}


			}
		}

}

//getLatestData("office");

updateRanks("office", "Regional", "YTD");
echo "\n<br>DONE Office Regional YTD" . runTime($startTime);
updateRanks("office", "Regional", "LTM");
echo "\n<br>DONE Office Regional LTM" . runTime($startTime);


updateRanks("office", "City", "YTD");
echo "\n<br>DONE Office City YTD" . runTime($startTime);
updateRanks("office", "City", "LTM");
echo "\n<br>DONE Office City LTM" . runTime($startTime);

updateRanks("office", "StateProvince", "YTD");
echo "\n<br>DONE Office StateProvince YTD" . runTime($startTime);
updateRanks("office", "StateProvince", "LTM");
echo "\n<br>DONE  Office StateProvince LTM" . runTime($startTime);


/*


// Generate the latest Region-wide ranking info
if ($newRegionalRanks) {
	foreach ($timePeriods as $timePeriod) {
		$select = "SELECT AssociateID, OfficeID, TotalCommission, StateProvinceName, FirstName, LastName 
					 FROM $associateTotalsTable 
					WHERE TimePeriod  = '$timePeriod'
					ORDER BY TotalCommission DESC";
		$DB->query($select);
		$DB->execute();
		$res = $DB->getResult();
		$rankCounter = 1;
		foreach ($res as $row) {
			$assocateID        = $row['AssociateID'];
			$officeID          = $row['OfficeID'];
			$stateProvinceName = $row['StateProvinceName'];
			$firstname         = addslashes($row['FirstName']);
			$lastname          = addslashes($row['LastName']);
			$insert            = "INSERT INTO $todayRankingTable (TimePeriod,AssociateID,FirstName,LastName,OfficeID,RegionalCommissionRank,StateProvinceName) 
						  			   VALUES('$timePeriod','$officeID','$assocateID','$firstname','$lastname',$rankCounter,'$stateProvinceName')
						  			   	   ON DUPLICATE KEY
						  			   UPDATE RegionalCommissionRank = $rankCounter";
			$DB->query($insert);
			$DB->execute();
			$rankCounter++;
		}

		echo "<br> Commission $timePeriod Rankings COMPLETE " . runTime($startTime);

		$select = "SELECT AssociateID, OfficeID, TotalEnds , FirstName, LastName
 					 FROM $associateTotalsTable 
 					WHERE TimePeriod  = '$timePeriod' 
 					ORDER BY TotalEnds DESC";
		$DB->query($select);
		$DB->execute();
		$res = $DB->getResult();
		$rankCounter = 1;
		foreach ($res as $row) {
			$assocateID = $row['AssociateID'];
			$officeID   = $row['OfficeID'];
			$firstname  = addslashes($row['FirstName']);
			$lastname   = addslashes($row['LastName']);
			$insert     = "INSERT INTO $todayRankingTable (TimePeriod,AssociateID,FirstName,LastName,OfficeID,RegionalEndsRank)
 						   		VALUES('$timePeriod','$assocateID','$firstname','$lastname','$officeID',$rankCounter)
 						   			ON DUPLICATE KEY
   						   		UPDATE RegionalEndsRank = $rankCounter";
			//echo "<br> $insert <br>";
			$DB->query($insert);
			$DB->execute();
			$rankCounter++;
		}

		echo "<br> Ends $timePeriod regional rankings COMPLETE " . runTime($startTime);


		$select = "SELECT AssociateID, OfficeID,TotalVolume, FirstName, LastName 
				 	 FROM $associateTotalsTable 
				 	WHERE TimePeriod  = '$timePeriod' 
				    ORDER BY TotalVolume DESC";
		$DB->query($select);
		$DB->execute();
		$res = $DB->getResult();
		$rankCounter = 1;
		foreach ($res as $row) {
			$assocateID = $row['AssociateID'];
			$officeID   = $row['OfficeID'];
			$firstname  = addslashes($row['FirstName']);
			$lastname   = addslashes($row['LastName']);
			$insert     = "INSERT INTO $todayRankingTable (TimePeriod,AssociateID,FirstName,LastName,OfficeID,RegionalVolumeRank) 
						  		VALUES('$timePeriod','$assocateID','$firstname','$lastname','$officeID',$rankCounter)
						 		    ON DUPLICATE KEY
						  		UPDATE RegionalVolumeRank = $rankCounter";

			//echo "<br> $insert <br>";

			$DB->query($insert);
			$DB->execute();
			$rankCounter++;
		}
		echo "<br> Volume $timePeriod regional rankings COMPLETE " . runTime($startTime);
	}
}

// Generate the latest office level ranks
if ($newOfficeRanks) {

	echo "Running for office ranks...<br>\n";

	foreach ($timePeriods as $timePeriod) {
		// Find all the unique offices and store them,
		$select = "SELECT DISTINCT OfficeID 
					 FROM $todayRankingTable";
		$DB->query($select);
		$DB->execute();
		$res = $DB->getResult();
		foreach ($res as $row) {
			$officeIDs[] = $row['OfficeID'];
		}

		// Loop through all office IDs and do the magic
		foreach ($officeIDs as $officeID) {

			// Office level Commission ranks
			// Get all the total commissions
			$select = "SELECT OfficeID, AssociateID, TotalCommission, FirstName, LastName 
						 FROM $associateTotalsTable 
						WHERE OfficeID = '$officeID' 
						ORDER BY TotalCommission desc ";
			//echo "$select";
			$DB->query($select);
			$DB->execute();
			$res = $DB->getResult();


			// Insert / update the commission ranks for offices.
			$rankCounter = 1;
			foreach ($res as $row) {
				//echo "RANK " . $row['AssociateID'] . " " . $rankCounter;
				$assocateID = $row['AssociateID'];
				$officeID   = $row['OfficeID'];
				$firstname  = addslashes($row['FirstName']);
				$lastname   = addslashes($row['LastName']);
				$inserts[]  = $insert = "INSERT INTO $todayRankingTable (TimePeriod,AssociateID,FirstName,LastName,OfficeID,OfficeCommissionRank) 
							  VALUES ('$timePeriod','$assocateID','$firstname','$lastname','$officeID',$rankCounter)
							  ON DUPLICATE KEY
							  UPDATE OfficeCommissionRank = $rankCounter ";

				//echo "<br>$insert<br>";


				$DB->query($insert);
				$DB->execute();
				//$DB->getResult();
				$rankCounter++;
			}

			// Office level Volume ranks
			// Get al lthe total volume ranks
			$select = "SELECT OfficeID, AssociateID, TotalVolume, FirstName, LastName 
						 FROM $associateTotalsTable 
						WHERE OfficeID = '$officeID' 
						ORDER BY TotalVolume desc ";
			//echo "$select";
			$DB->query($select);
			$DB->execute();
			$res = $DB->getResult();

			// Insert / update agent wide volume ranks
			$rankCounter = 1;
			foreach ($res as $row) {
				//echo "RANK " . $row['AssociateID'] . " " . $rankCounter;
				$assocateID = $row['AssociateID'];
				$officeID   = $row['OfficeID'];
				$firstname  = addslashes($row['FirstName']);
				$lastname   = addslashes($row['LastName']);
				$inserts[]  = $insert = "INSERT INTO $todayRankingTable (TimePeriod,AssociateID,FirstName,LastName,OfficeID,OfficeVolumeRank) 
											  VALUES ('$timePeriod','$assocateID','$firstname','$lastname','$officeID',$rankCounter)
											 	  ON DUPLICATE KEY
											  UPDATE OfficeVolumeRank = $rankCounter ";
				//echo "<br>$insert<br>";
				$DB->query($insert);
				$DB->execute();
				$rankCounter++;
			}

			//Office level Ends ranks

			// Get office level ends ranks
			$select = "SELECT OfficeID, AssociateID, TotalEnds, FirstName, LastName 
						 FROM $associateTotalsTable 
						WHERE OfficeID = '$officeID' 
						ORDER BY TotalEnds desc ";
			//echo "$select";
			$DB->query($select);
			$DB->execute();
			$res         = $DB->getResult();
			$rankCounter = 1;
			// Insert Update Office Ends ranks
			foreach ($res as $row) {
				//echo "RANK " . $row['AssociateID'] . " " . $rankCounter;
				$assocateID = $row['AssociateID'];
				$officeID   = $row['OfficeID'];
				$firstname  = addslashes($row['FirstName']);
				$lastname   = addslashes($row['LastName']);
				$inserts[]  = $insert = "INSERT INTO $todayRankingTable (TimePeriod,AssociateID,FirstName,LastName,OfficeID,OfficeEndsRank) 
							 				  VALUES ('$timePeriod','$assocateID','$firstname','$lastname','$officeID',$rankCounter)
							 				 	 	 			  ON DUPLICATE KEY
							 				  UPDATE OfficeEndsRank = $rankCounter ";
				//echo "<br>$insert<br>";

				$DB->query($insert);
				$DB->execute();
				$rankCounter++;
			}

		}
	}

}


// Generate the latest State / province ranks
if ($newStateProvinceRanks) {

	foreach ($timePeriods as $timePeriod) {

		$select = "SELECT DISTINCT StateProvinceName 
					 FROM $todayRankingTable";
		$DB->query($select);
		$DB->execute();
		$res = $DB->getResult();

		// Store all unique states or provinces
		foreach ($res as $row) {
			$stateProvinces[] = $row['StateProvinceName'];
		}

		foreach ($stateProvinces as $stateProvince) {

			// Provincial level Commission ranks

			if ($stateProvince == "") continue;
			$select = "SELECT StateProvinceName, OfficeID, AssociateID, TotalCommission, FirstName, LastName 
						 FROM $associateTotalsTable 
						WHERE StateProvinceName = '$stateProvince' 
						ORDER BY TotalCommission desc ";
			//echo "$select";
			$DB->query($select);
			$DB->execute();
			$res = $DB->getResult();

			// Insert or Update state / province level Commission ranks
			$rankCounter = 1;
			foreach ($res as $row) {
				//echo "RANK " . $row['AssociateID'] . " " . $rankCounter;
				$assocateID = $row['AssociateID'];
				$officeID   = $row['OfficeID'];
				$firstname  = addslashes($row['FirstName']);
				$lastname   = addslashes($row['LastName']);
				$inserts[]  = $insert = "INSERT INTO $todayRankingTable (TimePeriod,AssociateID,FirstName,LastName,OfficeID,StateProvinceCommissionRank,StateProvinceName) 
							 	 			  VALUES ('$timePeriod','$assocateID','$firstname','$lastname','$officeID',$rankCounter,'$stateProvince')
							 	 				  ON DUPLICATE KEY
							 	 			  UPDATE StateProvinceCommissionRank = $rankCounter ";

				//echo "<br>$insert<br>";
				$DB->query($insert);
				$DB->execute();
				$rankCounter++;
			}

			// Provincial level Volume ranks
			$select = "SELECT StateProvinceName, OfficeID, AssociateID, TotalVolume, FirstName, LastName 
						 FROM $associateTotalsTable 
						WHERE StateProvinceName = '$stateProvince' 
						ORDER BY TotalVolume desc ";
			//echo "$select";
			$DB->query($select);
			$DB->execute();
			$res = $DB->getResult();
			// Insert update rankings  for State / Province Volumes
			$rankCounter = 1;
			foreach ($res as $row) {
				echo "<br>";
				$assocateID = $row['AssociateID'];
				$officeID   = $row['OfficeID'];
				$firstname  = addslashes($row['FirstName']);
				$lastname   = addslashes($row['LastName']);
				$inserts[]  = $insert = "INSERT INTO $todayRankingTable (TimePeriod,AssociateID,FirstName,LastName,OfficeID,StateProvinceVolumeRank) 
											  VALUES ('$timePeriod','$assocateID','$firstname','$lastname','$officeID',$rankCounter)
												  ON DUPLICATE KEY
											  UPDATE StateProvinceVolumeRank = $rankCounter ";
				//echo "<br>$insert<br>";
				$DB->query($insert);
				$DB->execute();
				$rankCounter++;
			}
			// Provincial level Ends ranks
			$select = "SELECT StateProvinceName, OfficeID, AssociateID, TotalEnds, FirstName, LastName 
						 FROM $associateTotalsTable 
						WHERE StateProvinceName = '$stateProvince' 
						ORDER BY TotalEnds desc ";
			//echo "$select";
			$DB->query($select);
			$DB->execute();
			$res = $DB->getResult();
			$rankCounter = 1;
			foreach ($res as $row) {
				//echo "RANK " . $row['AssociateID'] . " " . $rankCounter;
				$assocateID = $row['AssociateID'];
				$officeID   = $row['OfficeID'];
				$firstname  = addslashes($row['FirstName']);
				$lastname   = addslashes($row['LastName']);
				$inserts[]  = $insert = "INSERT INTO $todayRankingTable (TimePeriod,AssociateID,FirstName,LastName,OfficeID,StateProvinceEndsRank) 
											  VALUES ('$timePeriod','$assocateID','$firstname','$lastname','$officeID',$rankCounter)
												  ON DUPLICATE KEY
											  UPDATE StateProvinceEndsRank = $rankCounter ";
				$DB->query($insert);
				$DB->execute();
				$rankCounter++;
			}

		}

	}
}

if ($newCityRanks) {

	foreach ($timePeriods as $timePeriod) {

		$log .= "<br>Updating City wide Ranks...<br>";

		$select = "SELECT DISTINCT OfficeCity 
					 FROM $associateTotalsTable";
		$DB->query($select);
		$DB->execute();
		$res = $DB->getResult();

		// Store all unique Office Cities
		foreach ($res as $row) {
			$cities[] = addslashes($row['OfficeCity']);
		}

		foreach ($cities as $city) {
			// Provincial level Commission ranks
			if ($city == "") continue;
			$select = "SELECT OfficeCity, OfficeID, AssociateID, TotalCommission, FirstName, LastName 
						 FROM $associateTotalsTable 
						WHERE OfficeCity = '$city' 
						ORDER BY TotalCommission desc ";
			//echo "$select";
			$DB->query($select);
			$DB->execute();
			$res = $DB->getResult();
			$rankCounter = 1;
			foreach ($res as $row) {
				$assocateID = $row['AssociateID'];
				$officeID   = $row['OfficeID'];
				$firstname  = addslashes($row['FirstName']);
				$lastname   = addslashes($row['LastName']);
				$inserts[]  = $insert = "INSERT INTO $todayRankingTable (TimePeriod,AssociateID,FirstName,LastName,OfficeID,CityCommissionRank,OfficeCity) 
											  VALUES ('$timePeriod','$assocateID','$firstname','$lastname','$officeID',$rankCounter,'$city')
												  ON DUPLICATE KEY
											  UPDATE StateProvinceCommissionRank = $rankCounter ";
				$DB->query($insert);
				$DB->execute();
				$rankCounter++;
			}
			// Provincial level Volume ranks
			$select = "SELECT OfficeCity, OfficeID, AssociateID, TotalVolume, FirstName, LastName 
						 FROM $associateTotalsTable 
						WHERE OfficeCity = '$city' 
						ORDER BY TotalVolume desc ";
			$DB->query($select);
			$DB->execute();
			$res = $DB->getResult();
			$rankCounter = 1;
			foreach ($res as $row) {
				echo "<br>";
				$assocateID = $row['AssociateID'];
				$officeID   = $row['OfficeID'];
				$firstname  = addslashes($row['FirstName']);
				$lastname   = addslashes($row['LastName']);
				$inserts[]  = $insert = "INSERT INTO $todayRankingTable (TimePeriod,AssociateID,FirstName,LastName,OfficeID,CityVolumeRank) 
											  VALUES ('$timePeriod','$assocateID','$firstname','$lastname','$officeID',$rankCounter)
												  ON DUPLICATE KEY
											  UPDATE CityVolumeRank = $rankCounter ";
				$DB->query($insert);
				$DB->execute();
				$rankCounter++;
			}
			// Provincial level Ends ranks
			$select = "SELECT OfficeCity, OfficeID, AssociateID, TotalEnds, FirstName, LastName 
						 FROM $associateTotalsTable 
						WHERE OfficeCity = '$city' 
						ORDER BY TotalEnds desc ";
			$DB->query($select);
			$DB->execute();
			$res = $DB->getResult();
			$rankCounter = 1;
			foreach ($res as $row) {
				echo "<br>";
				$assocateID = $row['AssociateID'];
				$officeID   = $row['OfficeID'];
				$firstname  = addslashes($row['FirstName']);
				$lastname   = addslashes($row['LastName']);
				$inserts[]  = $insert = "INSERT INTO $todayRankingTable (TimePeriod,AssociateID,FirstName,LastName,OfficeID,CityEndsRank) 
											  VALUES ('$timePeriod','$assocateID','$firstname','$lastname','$officeID',$rankCounter)
												  ON DUPLICATE KEY
											  UPDATE CityEndsRank = $rankCounter ";
				//echo "<br>$insert<br>";
				$DB->query($insert);
				$DB->execute();
				$rankCounter++;
			}
		}

	}
}

*/

// TODO: Add thermometer data.


echo "<br> (+) All Rankings COMPLETE " . runTime($startTime);

echo "<br> (+) SCRIPT COMPLETE " . runTime($startTime) . "<br>";

//$log = ob_get_contents();
//ob_end_clean();


?>
